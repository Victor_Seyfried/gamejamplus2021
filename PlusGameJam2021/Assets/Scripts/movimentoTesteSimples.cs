using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentoTesteSimples : MonoBehaviour
{
    Rigidbody mRigiBody;
    public float velocidadeMaxima = 5f;
    public string direcao;
    private bool pulando;
    void Start()
    {
        mRigiBody = GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        Vector3 movimentoInput = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 valorRotacao = transform.eulerAngles;
        
        if(movimentoInput.x == 0)// Sem A e D
        {
            if(movimentoInput.z > 0)//W
            {
                valorRotacao.y = Camera.main.transform.eulerAngles.y;
                transform.eulerAngles = valorRotacao;
                //transform.Translate (Vector3.forward * velocidadeMaxima * Time.deltaTime);
                mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
            }
            else if(movimentoInput.z < 0)//S
            {
                valorRotacao.y = Camera.main.transform.eulerAngles.y + 180;
                transform.eulerAngles = valorRotacao;
                mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
            }
        }
        else
        {
		    if (movimentoInput.x < 0)//A
		    {
			    if (movimentoInput.z > 0)//W
			    {
				    valorRotacao.y = Camera.main.transform.eulerAngles.y - 45;
				    transform.eulerAngles = valorRotacao;
                    
                    mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
				}
				else if (movimentoInput.z < 0)//S
				{
					valorRotacao.y = Camera.main.transform.eulerAngles.y - 135;
					transform.eulerAngles = valorRotacao;
                    
                    mRigiBody.MovePosition(transform.position +  transform.forward * Time.deltaTime * velocidadeMaxima);
				}
				else
				{
					valorRotacao.y = Camera.main.transform.eulerAngles.y - 90;
					transform.eulerAngles = valorRotacao;

                    mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
				}
			}
			else if (movimentoInput.x > 0)//D
			{
                if (movimentoInput.z > 0)//W
			    {
				    valorRotacao.y = Camera.main.transform.eulerAngles.y + 45;
					transform.eulerAngles = valorRotacao;
                    
                    mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
				}
				else if (movimentoInput.z < 0)//S
				{
					valorRotacao.y = Camera.main.transform.eulerAngles.y + 135;
					transform.eulerAngles = valorRotacao;
                    
                    mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
				}
				else
				{
					valorRotacao.y = Camera.main.transform.eulerAngles.y + 90;
					transform.eulerAngles = valorRotacao;

                    mRigiBody.MovePosition(transform.position + transform.forward * Time.deltaTime * velocidadeMaxima);
				}
			}
        }
    }
}
/*
public class mover : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 playerVelocity;
    private float playerSpeed = 2.0f;

    private void Start()
    {
        controller = gameObject.AddComponent<CharacterController>();
    }

    void Update()
    {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        controller.Move(move * Time.deltaTime * -playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }
        controller.Move(playerVelocity * Time.deltaTime);
    }
}*/