using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentoCamerav1 : MonoBehaviour
{
    [SerializeField] GameObject jogador;
    float velocidade;
    private float yaw = 0.0f;
    private float pitch = 0.0f;
    private float distanciaJogador;
    void Start()
    {
        velocidade = 2;
    }

    void Update()
    {
        transform.position = jogador.transform.position;
        yaw += velocidade * Input.GetAxis("Mouse X");
        pitch -= velocidade * Input.GetAxis("Mouse Y");
        float pitchFinal;
        pitchFinal = pitch;
        if(pitchFinal >= 90)
        {
            pitchFinal = 90;
        }
        else if(pitchFinal <= -90)
        {
            pitchFinal = -90;
        }
        transform.eulerAngles = new Vector3(pitchFinal, yaw, 0.0f);
    }
}